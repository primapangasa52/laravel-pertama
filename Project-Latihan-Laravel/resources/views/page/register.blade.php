@extends('layout.master')
@section('judul')
Halaman Biodata
<style type="text/css">
      h1 {
        font-family: Arial, Helvetica, sans-serif;
        color: blue;
      }
    </style>
@endsection
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/send" method="POST">
      @csrf
      <label for="firstname">First Name:</label><br />
      <input type="text" name="firstname" /><br />
      <br />
      <label for="lastname">Last Name:</label><br />
      <input type="text" name="lastname" /><br />
      <br />
      <label for="gender">Gender:</label> <br />
      <br />
      <input type="radio" name="gender" id="rad1" />Male <br />
      <input type="radio" name="gender" id="rad2" />Female <br />
      <input type="radio" name="gender" id="rad3" />Other <br />
      <br />
      <label for="nationality">Nationality:</label> <br />
      <br />
      <select name="nationality" id="nat">
        <option value="indonesian">Indonesian</option>
        <option value="france">France</option>
        <option value="united_states">United States</option>
        <option value="japan">Japan</option>
        <option value="australian">Autralian</option>
      </select>
      <br /><br />
      <label for="language">Language Spoken:</label> <br />
      <br />
      <input type="checkbox" name="language" id="lang1" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="language" id="lang2" />English
      <br />
      <input type="checkbox" name="language" id="lang3" />Other <br />
      <br />
      <label for="biodata">Biodata:</label> <br />
      <br />
      <textarea name="message" id="mes1" cols="30" rows="10"></textarea>
      <br /><br />
      <input type="submit" value="Sign Up" />
    </form>
@endsection
@extends('layout.master')
@section('judul')
Halaman Tambah Cast
<style type="text/css">
      h1 {
        font-family: Arial, Helvetica, sans-serif;
        color: blue;
      }
    </style>
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label >Nama</label>
    <input type="text" name="nama" class="form-control" >
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label >Umur</label>
    <input type="number" name="umur" class="form-control" min="0" >
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label >Biodata</label>
    <textarea name="bio" class="form-control"  cols="30" rows="10"></textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-success">Submit</button>
</form>

@endsection
@extends('layout.master')
@section('judul')
Halaman Edit Cast
<style type="text/css">
      h1 {
        font-family: Arial, Helvetica, sans-serif;
        color: blue;
      }
    </style>
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label >Nama</label>
    <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control" >
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label >Umur</label>
    <input type="number" name="umur" value="{{ $cast->umur }}" class="form-control" min="0" >
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label >Biodata</label>
    <textarea name="bio" class="form-control"  cols="30" rows="10">{{ $cast->bio }}</textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-success">Submit</button>
</form>

@endsection
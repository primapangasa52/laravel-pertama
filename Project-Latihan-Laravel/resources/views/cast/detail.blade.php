@extends('layout.master')
@section('judul')
Detail Cast
@endsection
@section('content')
<h3>{{ $cast->nama }}</h3>
<h6>{{ $cast->umur }}</h6>
<p>{{ $cast->bio }}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection
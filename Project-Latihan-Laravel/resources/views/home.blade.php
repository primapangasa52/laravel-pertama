
 @extends('layout.master')
 @section('content')
 @section('judul')
         <style type="text/css">
      h1 {
        font-family: Arial, Helvetica, sans-serif;
        color: blue;
      }
    </style>
    SanberBook
@endsection
@section('content')
    <article>
      <h2>Social Media Developer Santai Berkualitas</h2>
      <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    </article>

    <h2>Benefit Join di SanberBook</h2>
    <ul>
      <li>Mendapatkan motivasi dari sesama developer</li>
      <li>Sharing knowledge dari para mastah Sanber</li>
      <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h2>Cara Bergabung ke SanberBook</h2>
    <ol>
      <li>Mengunjungi Website ini</li>
      <li>
        Mendaftar di
        <a
          href="/register"
          title="Menuju ke Halaman Sign Up"
          style="color: blueviolet"
          >Form Sign Up</a
        >
      </li>
      <li>Selesai!</li>
    </ol>
@endsection

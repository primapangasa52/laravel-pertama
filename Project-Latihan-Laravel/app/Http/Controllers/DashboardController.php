<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //

    public function index()
    {
        return view('home');
    }

    public function register()
    {
        return view('page.register');
    }

    public function send(Request $request)
    {
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');

        return view('page.welcome', ['namadepan' => $firstname, 'namaakhir' => $lastname]);
    }
}

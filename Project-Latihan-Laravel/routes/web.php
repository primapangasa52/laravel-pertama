<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [DashboardController::class, 'index']);

Route::get('/register', [DashboardController::class, 'register']);

Route::post('/send', [DashboardController::class, 'send']);

Route::get('/data-table', function () {
    return view('page.data-table');
});

Route::get('/table', function () {
    return view('page.table');
});

// CRUD Cast
// Create
// Form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// Untuk kirim data ke database atau tambah data ke database
Route::post('/cast', [CastController::class, 'data']);


// Read
// Tampil semua data
Route::get('/cast', [CastController::class, 'index']);
// Detail Cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update
// Form update cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// Update data ke database berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete
// Delete berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
